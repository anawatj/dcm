package org.tao.dcm.domain.user;

import java.util.HashSet;
import java.util.Set;

import org.tao.dcm.domain.AbstractDomain;

public class Role extends AbstractDomain<Integer> {
		public Role()
		{
			this.permissions = new HashSet<Permission>();
		}
		private String roleCode;
		private String roleName;
		private Set<Permission> permissions;
		
		public Set<Permission> getPermissions() {
			return permissions;
		}
		public void setPermissions(Set<Permission> permissions) {
			this.permissions = permissions;
		}
		public String getRoleCode() {
			return roleCode;
		}
		public void setRoleCode(String roleCode) {
			this.roleCode = roleCode;
		}
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
		
}
