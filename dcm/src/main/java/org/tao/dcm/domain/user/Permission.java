package org.tao.dcm.domain.user;

import org.tao.dcm.domain.AbstractDomain;

public class Permission extends AbstractDomain<Integer> {

	public Permission()
	{
		
	}
	private String permissionCode;
	private String permissionName;
	public String getPermissionCode() {
		return permissionCode;
	}
	public void setPermissionCode(String permissionCode) {
		this.permissionCode = permissionCode;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	
}
