package org.tao.dcm.domain.container;

import java.util.HashSet;
import java.util.Set;

import org.tao.dcm.domain.AbstractDomain;
import org.tao.dcm.domain.master.Branch;

public class Container extends AbstractDomain<Integer> {
		
	public Container()
	{
		this.items = new HashSet<Container>();
	}
	private String containerCode;
	private String containerDesc;
	private Branch branch;
	private Set<Container> items;
	public String getContainerCode() {
		return containerCode;
	}
	public void setContainerCode(String containerCode) {
		this.containerCode = containerCode;
	}
	public String getContainerDesc() {
		return containerDesc;
	}
	public void setContainerDesc(String containerDesc) {
		this.containerDesc = containerDesc;
	}
	public Branch getBranch() {
		return branch;
	}
	public void setBranch(Branch branch) {
		this.branch = branch;
	}
	public Set<Container> getItems() {
		return items;
	}
	public void setItems(Set<Container> items) {
		this.items = items;
	}
	
}
