package org.tao.dcm.domain.deposit;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.tao.dcm.domain.AbstractDomain;
import org.tao.dcm.domain.master.Branch;

public class Deposit extends AbstractDomain<Integer> {
		public Deposit()
		{
			this.documents = new  HashSet<Document>();
		}
		private String depositCode;
		private String depositDesc;
		private Date depositDate;
		private Branch branch;
		private String reference;
		private Set<Document> documents;
		
		public String getDepositCode() {
			return depositCode;
		}
		public void setDepositCode(String depositCode) {
			this.depositCode = depositCode;
		}
		public String getDepositDesc() {
			return depositDesc;
		}
		public void setDepositDesc(String depositDesc) {
			this.depositDesc = depositDesc;
		}
		public Date getDepositDate() {
			return depositDate;
		}
		public void setDepositDate(Date depositDate) {
			this.depositDate = depositDate;
		}
		public Branch getBranch() {
			return branch;
		}
		public void setBranch(Branch branch) {
			this.branch = branch;
		}
		public String getReference() {
			return reference;
		}
		public void setReference(String reference) {
			this.reference = reference;
		}
		public Set<Document> getDocuments() {
			return documents;
		}
		public void setDocuments(Set<Document> documents) {
			this.documents = documents;
		}
		
}
